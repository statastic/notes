package io.statastic.notes.repository;


import io.statastic.notes.model.Note;
import io.statastic.notes.model.Tag;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.UUID;

public class RepositoryTest {

    private static EntityManager em;
    private static final Repository repository = new Repository();

    @BeforeAll
    public static void setUp() throws Exception {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("notes");
        em = emf.createEntityManager();
        Field field = Repository.class.getDeclaredField("entityManager");
        field.setAccessible(true);
        field.set(repository, em);
    }

    @Test
    public void test() {
        em.getTransaction().begin();
        Note n = new Note(UUID.randomUUID().toString(), "foo");
        Tag t = new Tag(UUID.randomUUID().toString(), "tag");
        n.getTags().add(t);
        em.persist(n);
        em.flush();
        em.getTransaction().commit();
    }

    @Test
    public void testReuseTag() {
        em.getTransaction().begin();
        Tag t = new Tag(UUID.randomUUID().toString(), "a");
        em.persist(t);
        em.getTransaction().commit();

        em.getTransaction().begin();
        Note n = new Note(UUID.randomUUID().toString(), "foo");
        t = em.find(Tag.class, t.getId());
        n.getTags().add(t);
        em.persist(n);
        em.getTransaction().commit();
    }

    @Test
    public void testQuery() {
        Collection<Note> allNotes = repository.getAllNotes();
        System.out.println(allNotes);
    }
}
