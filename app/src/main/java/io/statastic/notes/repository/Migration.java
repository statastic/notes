package io.statastic.notes.repository;

import jakarta.annotation.Resource;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Initialized;
import jakarta.enterprise.event.Observes;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class Migration {
    private final Logger logger = LoggerFactory.getLogger(Migration.class);

    @Resource(lookup = "java:jboss/jdbc/NotesDS")
    private DataSource dataSource;

    public void upgrade(@Observes @Initialized(ApplicationScoped.class) Object ignored) {
        logger.info("Migration upgrade!");
        Map<String,String> config = new HashMap<>();
        config.put("flyway.locations", "classpath:db/migration");
        // to create schema history table
        config.put("flyway.baselineOnMigrate", Boolean.TRUE.toString());
        Flyway flyway = Flyway
                .configure()
                .configuration(config)
                .dataSource(dataSource).
                load();
        flyway.migrate();
    }
}
