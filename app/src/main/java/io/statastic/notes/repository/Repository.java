package io.statastic.notes.repository;

import io.statastic.notes.model.Note;
import io.statastic.notes.model.Tag;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.*;

@ApplicationScoped
public class Repository {

    @PersistenceContext(unitName = "notes")
    private EntityManager entityManager;

    public Collection<Note> getAllNotes() {
        TypedQuery<Note> query = entityManager.createQuery("select n from Note n", Note.class);
        return query.getResultList();
    }

    public Tag getOrCreateTag(String tag){
        TypedQuery<Tag> query = entityManager.createQuery("select t from Tag t where t.tag = :tag", Tag.class);
        query.setParameter("tag", tag.toLowerCase());

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return createTag(tag);
        }
    }

    public List<Tag> getAllTags() {
        TypedQuery<Tag> query = entityManager.createQuery("select t from Tag t order by t.tag", Tag.class);
        return query.getResultList();
    }

    public Tag createTag(String tag) {
        Tag t = new Tag(UUID.randomUUID().toString(), tag);
        entityManager.persist(t);
        return t;
    }

    public void saveNote(Note n) {
        n.setCreated(LocalDateTime.now());
        entityManager.persist(n);
    }

    public void updateNote(Note n) {
        entityManager.merge(n);
    }

    public void deleteNote(String id) {
        Note note = entityManager.find(Note.class, id);
        note.getTags().clear();
        entityManager.remove(note);
    }

    public Note getNote(String noteId) {
        return entityManager.find(Note.class, noteId);
    }
}
