package io.statastic.notes.services;

import io.statastic.notes.repository.Repository;
import org.primefaces.shaded.json.JSONArray;

import jakarta.inject.Inject;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Simple Servlet that dumps all Notes to Json.
 *
 * This is a quick and dirty solution for a simple data backup.
 */
@WebServlet(name="dumpData", urlPatterns = "/dumpData")
public class DumpDataServlet extends HttpServlet {

    @Inject
    private Repository repository;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        JSONArray jsonObject = new JSONArray(repository.getAllNotes());
        jsonObject.write(response.getWriter());
    }
}
