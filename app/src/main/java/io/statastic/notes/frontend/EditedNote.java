package io.statastic.notes.frontend;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EditedNote {

    private String id;
    private String text;

    private List<String> tags = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
