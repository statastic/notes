package io.statastic.notes.frontend;


import io.statastic.notes.model.Note;
import io.statastic.notes.model.Tag;
import io.statastic.notes.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.transaction.Transactional;
import java.io.Serializable;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Named("overview")
@SessionScoped
@Transactional
public class Overview implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Overview.class);

    private EditedNote editedNote = new EditedNote();

    @Inject
    private Repository repository;

    public String save() {
        if (editedNote != null) {
            return update();
        }
        return "index?faces-redirect=true";
    }

//    private String saveNewNote() {
//
//        if (!hasWritePermission()) {
//            return "index?faces-redirect=true";
//        }
//
//        if (newNote.getText() == null || newNote.getText().trim().isEmpty()) {
//            return "index?faces-redirect=true";
//        }
//
//        Note n = toNote(newNote);
//        n.setId(UUID.randomUUID().toString());
//
//        repository.saveNote(n);
//        newNote = new EditedNote();
//        return "index?faces-redirect=true";
//    }

    private String update() {
        if (!hasWritePermission()) {
            return "index?faces-redirect=true";
        }

        if (editedNote == null) {
            return "index?faces-redirect=true";
        }

        if (editedNote.getText() == null || editedNote.getText().trim().isEmpty()) {
            return "index?faces-redirect=true";
        }

        if (editedNote.getId() == null) {
            editedNote.setId(UUID.randomUUID().toString());
            repository.saveNote(toNote(editedNote));
        } else {
            repository.updateNote(toNote(editedNote));
        }


        editedNote = new EditedNote();
        return "index?faces-redirect=true";
    }

    public String edit(String noteId) {
        if (!hasWritePermission()) {
            return "index?faces-redirect=true";
        }
        Note note = repository.getNote(noteId);
        editedNote = fromNote(note);
        return "index?faces-redirect=true";
    }

    public String delete(String id) {
        if (!hasWritePermission()) {
            return "index?faces-redirect=true";
        }
        repository.deleteNote(id);
        return "index?faces-redirect=true";
    }



    public String cancelEditing() {
        if (!hasWritePermission()) {
            return "index?faces-redirect=true";
        }
        this.editedNote = null;
        return "index?faces-redirect=true";
    }

    public Collection<Note> getNotes() {
        return repository.getAllNotes();
    }

    public String getUser() {
        Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
        if (principal != null) {
            return principal.getName();
        } else {
            return "not logged in";
        }
    }

    public boolean hasWritePermission() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("default-roles-statastic");
    }

    public boolean isEditing(String noteId) {
        return editedNote != null && Objects.equals(editedNote.getId(), noteId);
    }

    public EditedNote getEditedNote() {
        return editedNote;
    }

    public void setEditedNote(EditedNote editedNote) {
        this.editedNote = editedNote;
    }

    private EditedNote fromNote(Note n) {
        EditedNote editedNote = new EditedNote();
        editedNote.setId(n.getId());
        editedNote.setText(n.getText());
        List<String> tags = n.getTags().stream()
                .map(Tag::getTag)
                .collect(Collectors.toList());
        editedNote.setTags(tags);
        return editedNote;
    }

    private Note toNote(EditedNote note) {
        Note n = new Note();
        n.setId(note.getId());
        n.setText(note.getText());
        note.getTags().stream()
                .map(String::trim)
                .map(String::toLowerCase)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toSet())
                .forEach( s -> {
                    Tag t = repository.getOrCreateTag(s);
                    n.getTags().add(t);
                });
        return n;
    }

    public boolean getShowNewNote() {
         return hasWritePermission() && editedNote.getId() == null;
    }
}