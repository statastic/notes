# notes

a simple app to keep notes

# start test environment
```
docker-compose -f app/src/test/docker/docker-compose.yml up -d
```

# deploy to test environment
```
mvn clean install
```

# login to test environment
- url: http://wildfly:18080/notes/
- user: boimler
- password: lowerdecks